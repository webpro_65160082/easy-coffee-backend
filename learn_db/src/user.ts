import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
const userRepository = AppDataSource.getRepository(User);
const roleRepository = AppDataSource.getRepository(Role);

const adminRole = await roleRepository.findOneBy({id:1})
const userRole = await roleRepository.findOneBy({id:2})
console.log("Inserting a new user into the Memory...")
    await userRepository.clear()
    var user = new User()
    user.id = 1
    user.email = "admin@email.com"
    user.password = "pass1234"
    user.gender = "male"
    user.roles=[]
    user.roles.push(adminRole)
    console.log("Inserting a new user into the Database...")
    await userRepository.save(user)

    
    user = new User()
    user.id = 2
    user.email = "user1@email.com"
    user.password = "pass1234"
    user.gender = "male"
    user.roles=[]
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await userRepository.save(user)

  

    user = new User()
    user.id = 3
    user.email = "user2@email.com"
    user.password = "pass1234"
    user.gender = "male"
    user.roles=[]
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await userRepository.save(user)

    const users= await userRepository.find({relations:{roles:true}})
    console.log(users);

    const roles = await roleRepository.find({relations:{users:true}})
    console.log(roles);
    
}).catch(error => console.log(error))
