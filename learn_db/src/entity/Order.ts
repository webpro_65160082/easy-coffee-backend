import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable, OneToMany, ManyToOne } from "typeorm"
import { OrderItem } from "./OrderItem"
import { User } from "./User"


@Entity()
export class Order {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    total: number

    @Column()
    qty: number

    @CreateDateColumn()
    create: Date

    @UpdateDateColumn()
    update: Date

    @OneToMany(()=> OrderItem,(orderItem)=> orderItem.order)
    orderItems:OrderItem[]
    
    @ManyToOne(()=>User,(user)=>user.orders)
    user: User
}
