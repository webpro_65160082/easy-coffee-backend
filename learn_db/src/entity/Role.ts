import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, ManyToMany } from "typeorm"
import { User } from "./User"

@Entity()
export class Role {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @CreateDateColumn()
    create: Date

    @UpdateDateColumn()
    update: Date

    @ManyToMany(()=> User,(user)=>user.roles)
    users:User
}
